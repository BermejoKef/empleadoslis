<?php
    $errores = array();
    $id ="";
	$name = "";
	$last_name = "";
	$email = "";
	$phone = "";
    $acc="";
    $exito = array();
require "php/conn.php";
require "php/funciones.php";
//Modo de la página
//S - consulta
//A - alta
//B - borrar
//C - cambiar
if (isset($_GET["m"])) {
	$m = $_GET["m"];
} else {
	$m = "S";
}
//lee la tabla productos
if ($m=="D") {
	$id = $_GET["id"];
		//Borramos el registro
		$sql = "DELETE FROM employees WHERE id=".$id;
		if(mysqli_query($conn, $sql)){
            header("location:index.php");
		}
		$errores = array("Error al borrar al empleado");

        print $sql;
}
//Detectamos si se envía la información
if(isset($_POST["name"])){
	//Recuperamos el identificador
	$id = $_POST["id"];
   // $id = $_GET["id"];
	//Recuperamos la información del usuario
	$name = $_POST["name"];
	$last_name = $_POST["last_name"];
	$email = $_POST["email"];
	$phone = $_POST["phone"];
    $acc =$_POST["acc"];
	//Armamos el SQL
if (is_valid_email($email)!=false){
if(validaCorreo2($email, $conn,$id)){
if(validaPhone2($phone, $conn,$id)){
	$sql = "UPDATE employees SET ";
	$sql .= "name='".$name."', ";
	$sql .= "last_name='".$last_name."', ";
	$sql .= "email='".$email."', ";
	$sql .= "phone='".$phone."' ";
	$sql .= "WHERE id=".$id;
	//Ejecutamos el sql
	if(mysqli_query($conn, $sql)){

    $sq = "SELECT COUNT(*) as total FROM `acciones` WHERE id_em=".$id;
   $r = mysqli_query($conn, $sq);
	//pasamos los datos a un objeto
	$data = mysqli_fetch_assoc($r);
	//Variables de trabajo
	$total = $data["total"];
    if($total==1){
    $sq = "UPDATE acciones SET ";
	$sq .= "acc='".$acc."' ";
	$sq .= "WHERE id_em=".$id;
    mysqli_query($conn, $sq);
    }else{
	//Ejecutamos el sql
    $sq = " INSERT INTO acciones(acc, id_em) VALUES(";
	$sq .= "'".$acc."', ";
	$sq .= "".$id.")";
    mysqli_query($conn, $sq);
    }
		array_push($exito, "Se edito con exito a:".$name." ".$last_name."con el id:".$id."");
        /*
        header("location:index.php");
		exit;
        */
	}
}else{array_push($errores,"Ya existe el telefono: ".$phone);}
}else{array_push($errores,"Ya existe el correo:".$email);}
}else{array_push($errores, "El email esta incorrecto"); }

}
//lee la tabla
if ($m=="S") {
	$sql = "SELECT id,name,last_name,email,phone,acc FROM employees A LEFT OUTER JOIN acciones B ON A.id = B.id_em order by A.id";
	$r = mysqli_query($conn, $sql);
	$empleados = array();
	while($data = mysqli_fetch_assoc($r)){
		array_push($empleados, $data);
	}
}
//lee un empleado
if ($m=="C") {
	$id = $_GET["id"];
	//Leemos el registro del empleado
	//$sql = "SELECT id,name,last_name,email,phone,acc FROM employees A JOIN acciones B WHERE A.id=".$id." and B.id_em=".$id;
    $sql = "SELECT id,name,last_name,email,phone FROM employees WHERE id =".$id;
	$r = mysqli_query($conn, $sql);
	//pasamos los datos a un objeto
	$data = mysqli_fetch_assoc($r);
	//Variables de trabajo
	$name = $data["name"];
	$last_name = $data["last_name"];
	$email = $data["email"];
	$phone = $data["phone"];

    $sq = "SELECT acc FROM acciones WHERE id_em =".$id;
	$r2 = mysqli_query($conn, $sq);
	//pasamos los datos a un objeto
	$data2 = mysqli_fetch_assoc($r2);
    $acc = $data2["acc"];
}
if ($m=="B") {
	$id = $_GET["id"];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Empleados</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="styles.css" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="script.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="index.php" class="navbar-brand">Lista</a>
		</div>
		<div class="collapse navbar-collapse" id="menu">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php">Empleados</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php require "php/navbar.php"; ?>
			</ul>
		</div>
	</div>
</nav>

<div class="container-fluid text-center">
	<div class="row content">
		<div class="col-sm-2 sidenav">
		</div>
		<div class="col-sm-8 text-left">
			<div class="well" id="contenedor">
				<h2 class="text-center">Empleados</h2>
				<?php
                 if(count($errores)>0){
					print '<div class="alert alert-danger">';
					foreach ($errores as $key => $valor) {
						print "<strong>* ".$valor."</strong>";
					}
					print '</div>';
				}
                if(count($exito)>0){
					print '<div class="alert alert-success">';
					foreach ($exito as $key => $valores) {
						print "<strong>* ".$valores."</strong>";
					}
					print '</div>';
				}

				if($m=="C"){

				?>
				<form action="index.php" method="post">
					<div class="form-group text-left">
						<label for="nombre">* Nombre:</label>
						<input type="text" name="name" id="name" class="form-control" required placeholder="Escriba su nombre" value="<?php print $name; ?>"/>
					</div>

					<div class="form-group text-left">
						<label for="apellidoPaterno">* Apellido :</label>
						<input type="text" name="last_name" id="last_name" class="form-control" required placeholder="Escriba su apellido " value="<?php print $last_name; ?>"/>
					</div>


					<div class="form-group text-left">
						<label for="correo">* Correo electrónico:</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Escriba su correo electrónico"  value="<?php print $email; ?>"/>
					</div>

					<div class="form-group text-left">
						<label for="direccion">* Telefono:</label>
						<input type="number" name="phone" id="phone" class="form-control" placeholder="Escriba su dirección"  value="<?php print $phone; ?>"/>
					</div>
                    <div class="form-group text-left">
						<label for="acc">Acciones:</label>
                        <textarea class="form-control" name="acc" id="acc" ><?php print $acc ?></textarea>
					</div>

					<input type="hidden" id="id" name="id" value="<?php print $id; ?>">

					<div class="form-group text-left">
						<label for="enviar"></label>
						<input type="submit" name="enviar" value="Enviar" class="btn btn-success" role="button"/>
					</div>

				</form>
				<?php }
                if($m=="B"){
					print '<div class="alert alert-danger">';
					print "¿Desea borrar al empleado?";
					print "<a href='index.php'>No</a>&nbsp;";
					print "<a href='index.php?m=D&id=".$id."'>Si</a>";
					print "</div>";
				}
				if ($m=="S") {
                    //print "<div class='table-responsive-md'>";
                    print "<input id='searchTerm' type='text' onkeyup='doSearch()'  placeholder='Buscar'/> <br><br>";
                    print "<div id='elsc' style='overflow-x:auto; overflow-y:50%;'>";
					print "<table id='dtBasicExample' class='table table-striped table-bordered table-sm' cellspacing=0 width='100%'>";
					print "<tr>";
					print "<th>id</th>";
					print "<th>Nombre</th>";
					print "<th>Apellido</th>";
					print "<th>Email</th>";
                    print "<th>Phone</th>";
                    print "<th>Acciones</th>";
					print "<th>Modificar</th>";
					print "<th>Borrar</th>";
					print "</tr>";
					for ($i=0; $i < count($empleados) ; $i++) {
						print "<tr>";
						print "<td>".$empleados[$i]["id"]."</td>";
						print "<td>".$empleados[$i]["name"]."</td>";
						print "<td>".$empleados[$i]["last_name"]."</td>";
						print "<td>".$empleados[$i]["email"]."</td>";
                        print "<td>".$empleados[$i]["phone"]."</td>";
                        print "<td>".$empleados[$i]["acc"]."</td>";
						print "<td><a class='btn btn-info' href='index.php?m=C&id=".$empleados[$i]["id"]."'>Modificar</a></td>";
						print "<td><a class='btn btn-danger' href='index.php?m=B&id=".$empleados[$i]["id"]."'>Borrar</a></td>";
						print "</tr>";
					}
					print "</table>";
                    print "</div>";
				}
				?>
			</div>
            <form action="agempleados.php" method="post" enctype="multipart/form-data">
            <div class="form-group text-left">
                <input type="submit" name="nuevo" value="Nuevo Empleado" class="btn btn-info btn1" role="button"/>
					</div>
            </form>
		</div>

	</div>
</div>

<footer class="container-fluid text-center">
</footer>
</body>
</html>
