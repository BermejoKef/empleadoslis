<?php
//Iniciar la sesion
$errores = array();
$exito = array();
	//Validación
	//Recuperamos el id
	$id = "";
	$name = "";
	$last_name = "";
	$email = "";
	$phone = "";
    $acc ="";
require "php/conn.php";
require "php/funciones.php";

/*if (isset($_GET["m"])) {
	$m = $_GET["m"];
} */
if(isset($_POST["name"])){
	//Variables de trabajo
	$errores = array();
	//Validación
	//Recuperamos el id
	$id = (isset($_POST["id"]))?$_POST["id"]:"";
	$name = $_POST["name"];
	$last_name = $_POST["last_name"];
	$email = $_POST["email"];
	$phone = $_POST["phone"];
    $acc = $_POST["acc"];
	//validación
	if ($name=="") {
		array_push($errores, "El nombre es requerido");
	} else if ($last_name=="") {
		array_push($errores, "El apellido es requerido");
	} else if ($email=="") {
		array_push($errores, "El email es requerido");
	} else if (is_valid_email($email)==false){
        array_push($errores, "El email esta incorrecto");
    } else if ($phone=="") {
		array_push($errores, "El telefono es requerido");
	}else if(valid_phone_number_or_empty($phone)==false){
        array_push($errores, "El telefono no es incorrecto");
    } else {
        if(validaCorreo($email, $conn)){
        if(validaPhone($phone, $conn)){
 			$sql = "INSERT INTO employees (name, last_name, email, phone)VALUES('".$name."', ";
			$sql .= "'".$last_name."', ";
			$sql .= "'".$email."', ";
			$sql .= $phone.")";
		if(!mysqli_query($conn, $sql)){
			array_push($errores,"Error al insertar el registro ");
		}else{
            $sq = "INSERT INTO acciones (id_em, acc)VALUES((SELECT id FROM employees WHERE name='".$name."' AND last_name='".$last_name."' AND email='".$email."' AND phone=".$phone."),'".$acc."')";
		if(!mysqli_query($conn, $sq)){
			array_push($errores,"Error al insertar su accion ");
            print $sq;
		}
            array_push($exito, "Nuevo empleado agregado con exito");

            $_POST['name']="";
            $_POST['last_name']="";
            $_POST['email']="";
            $_POST['phone']="";
        }


	}else{array_push($errores,"Ya existe el telefono: ".$phone);}
    }else{array_push($errores,"Ya existe el correo:".$email);}
}
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Empleados</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script>
        $('#myForm').submit(function(e) {
  e.preventDefault();

  // valida la forma, aquí puedes hacer los envíos por AJAX
  // ejemplo para saber si el id del decreto es válido, envía el valor
  // del decreto y revísalo por php, así se enviaría con JS

  var decretoId = $('#decreto').val();

  $.ajax({ url: '#?decreto=', type: 'GET', dataType: 'json' })
  .success(function(data) {
    // suponiendo que php ha devuelto un json que dice { "valid": false }
    if(!data.valid) {
      decreto.val('');
    }
  });
});
	</script>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="index.php" class="navbar-brand">Lista</a>
		</div>
		<div class="collapse navbar-collapse" id="menu">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Empleados</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?php require "php/navbar.php"; ?>
			</ul>
		</div>
	</div>
</nav>

<div class="container-fluid text-center">
	<div class="row content">
		<div class="col-sm-2 sidenav">
		</div>
		<div class="col-sm-8 text-left">
			<div class="well" id="contenedor">
				<h2 class="text-center">Empleados</h2>
                <form id="myForm" action="agempleados.php" method="post">
                <?php
					if(count($errores)>0){
						print '<div class="alert alert-danger">';
						foreach ($errores as $key => $valor) {
							print "<strong>* ".$valor."</strong>";
						}
						print '</div>';
					}
                    if(count($exito)>0){
						print '<div class="alert alert-success">';
						foreach ($exito as $key => $valores) {
							print "<strong>* ".$valores."</strong>";
						}
						print '</div>';
					}
				?>

					<div class="form-group text-left">
						<label for="nombre">Nombre:</label>
						<input type="text" name="name" id="name" class="form-control"  placeholder="Escriba su nombre"value="<?php if(isset($_POST['name'])){echo $_POST['name'];}?>"/>
					</div>

					<div class="form-group text-left">
						<label for="apellidoPaterno">Apellido :</label>
						<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Escriba su apellido" value="<?php if(isset($_POST['last_name'])){echo $_POST['last_name'];}?>"/>
					</div>


					<div class="form-group text-left">
						<label for="correo">Email:</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Escriba su correo electrónico" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>"/>
					</div>

					<div class="form-group text-left">
						<label for="direccion">Telefono:</label>
						<input type="number" name="phone" id="phone" class="form-control" placeholder="Numero de telefono" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];}?>"/>
					</div>
                    <div class="form-group text-left">
						<label for="acc">Acciones:</label>
                        <textarea class="form-control" name="acc" id="acc" ><?php if(isset($_POST['acc'])){echo $_POST['acc'];}?></textarea>
					</div>


					<input type="hidden" id="id" name="id" value="<?php print $id; ?>">

					<div class="form-group text-left">
						<label for="enviar"></label>
						<input type="submit" name="enviar" value="Enviar" class="btn btn-success" role="button"/>
					</div>
				</form>
			</div>
            <div class="form-group text-left">
                <form action="index.php" method="post" enctype="multipart/form-data">
            <div class="form-group text-left">
                <input type="submit" name="regresar" value="Regresar" class="btn btn-info btn1" role="button"/>
					</div>
            </form>
					</div>
		</div>
		<div class="col-sm-2 sidenav">

		</div>
	</div>
</div>

<footer class="container-fluid text-center">
</footer>
</body>
</html>
