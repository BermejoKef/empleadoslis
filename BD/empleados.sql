-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2020 a las 04:58:37
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empleados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

CREATE TABLE `acciones` (
  `id_acc` int(11) NOT NULL,
  `acc` varchar(200) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `id_em` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acciones`
--

INSERT INTO `acciones` (`id_acc`, `acc`, `id_em`) VALUES
(1, 'No se que acciones puede tener', 3),
(2, 'porqueeeee', 34),
(18, 'hola hola ya quedo ', 28),
(19, '', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `last_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `phone` varchar(10) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `employees`
--

INSERT INTO `employees` (`id`, `name`, `last_name`, `email`, `phone`) VALUES
(1, 'Allan', 'Poe', 'allan_poe_09@email.com', '4435679083'),
(2, 'Clive S.', 'Lewis', 'cs_lewis_98@email.com', '4437094524'),
(3, 'JK', 'Rowling', 'jk_rowling_65@email.com', '4436122456'),
(4, 'Cassandra', 'Clare', 'cassandra_clare_73@email.com', '4437972345'),
(5, 'Stephen', 'King', 'stephen_king_47@email.com', '4438996523'),
(6, 'Kevin', 'Bermejo', 'kevin_black_96@hotmail.com', '4431050233'),
(27, 'Kevin', 'Bermejo', 'kevin_black_45496@hotmail.com', '5556661213'),
(28, 'Kevin', 'Bermejoas', 'kevin_blaasdck_96@hotmail.com', '2147483190'),
(32, 'Alexs', 'Barajass', 'ale_baasar96as@gmail.com', '5541005523'),
(33, 'Aaron', 'Contreras', 'aaron_baasar96as@gmail.com', '3332225544'),
(34, 'Alfonso', 'Barajas', 'alfo_dr56@gmail.com', '4546669845');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD PRIMARY KEY (`id_acc`),
  ADD KEY `id_em` (`id_em`);

--
-- Indices de la tabla `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acciones`
--
ALTER TABLE `acciones`
  MODIFY `id_acc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD CONSTRAINT `acciones_ibfk_1` FOREIGN KEY (`id_em`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
